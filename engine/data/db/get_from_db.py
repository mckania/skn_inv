from db import test_con_db
import logging, os


''' API endpoints are calling these methods in order to receive data from DB '''

logging.getLogger(__name__)

def get_tables():
    cursor, connection = test_con_db.set_cursor()
    if cursor:
        cursor.execute("SELECT relname FROM pg_class WHERE relkind='r' AND relname !~ '^(pg_|sql_)';")

        logging.info(cursor.query)
        logging.info(cursor.statusmessage)

        return tuple(cursor.fetchall())
    else:
        return ''

def get_items():
    cursor, connection = test_con_db.set_cursor()
    if cursor:
        cursor.execute("SELECT name FROM items;")

        logging.info(cursor.query)
        logging.info(cursor.statusmessage)

        return tuple(cursor.fetchall())
    else:
        return ''

def get_rooms():
    cursor, connection = test_con_db.set_cursor()
    if cursor:
        cursor.execute("SELECT name FROM rooms;")

        logging.info(cursor.query)
        logging.info(cursor.statusmessage)

        return tuple(cursor.fetchall())
    else:
        return ''

def get_item_by_id(id: str):
    cursor, connection = test_con_db.set_cursor()
    if cursor:
        cursor.execute("SELECT * FROM items WHERE id = '{0}';".format(str(id)))

        logging.info(cursor.query)
        logging.info(cursor.statusmessage)

        return tuple(cursor.fetchall())
    else:
        return ''

def get_room_by_id(id: str):
    cursor, connection = test_con_db.set_cursor()
    if cursor:
        cursor.execute("SELECT * FROM rooms WHERE id = '{0}';".format(str(id)))

        logging.info(cursor.query)
        logging.info(cursor.statusmessage)

        return tuple(cursor.fetchall())
    else:
        return ''

def get_reserved():
    cursor, connection = test_con_db.set_cursor()
    if cursor:
        result = []

        cursor.execute("SELECT * FROM items WHERE items.reserved_by IS NOT NULL;")
        result.append(cursor.fetchall())

        logging.info(cursor.query)
        logging.info(cursor.statusmessage)

        cursor.execute("SELECT * FROM rooms WHERE rooms.reserved_by IS NOT NULL;")
        result.append(cursor.fetchall())

        logging.info(cursor.query)
        logging.info(cursor.statusmessage)

        return result
    else:
        return ''

def get_unreserved():
    cursor, connection = test_con_db.set_cursor()
    if cursor:
        result = []

        cursor.execute("SELECT * FROM items WHERE items.reserved_by IS NULL;")
        result.append(cursor.fetchall())

        logging.info(cursor.query)
        logging.info(cursor.statusmessage)

        cursor.execute("SELECT * FROM rooms WHERE rooms.reserved_by IS NULL;")
        result.append(cursor.fetchall())

        logging.info(cursor.query)
        logging.info(cursor.statusmessage)

        return result
    else:
        return ''



