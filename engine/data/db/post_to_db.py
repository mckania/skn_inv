from db import test_con_db
from db.request_items import *
import logging, datetime, uuid
from psycopg2.extras import UUID_adapter


''' API endpoints are calling these methods in order to perform actions in DB '''

'''TODO: Consider refactoring these methods to DRY or replace with Class '''

logging.getLogger(__name__)

def reserve_item(item: Item, user: User, time: Time, response_model = Response):
    cursor, conn = test_con_db.set_cursor()     
    if cursor:
        item.iid = UUID_adapter(item.iid)       

        cursor.execute("""
                            UPDATE items
                            SET reserved_by = %(reserved_by)s, reserved_from = %(datefrom)s, reserved_until = %(dateuntil)s
                            WHERE name = %(name)s AND id = %(iid)s AND reserved_by IS NULL AND reserved_from IS NULL AND reserved_until IS NULL; """,
                            ({'name': item.name, 'iid': item.iid, 'reserved_by': user.name + ':' + str(user.uid), 
                                'datefrom': time.reserved_from, 'dateuntil': time.reserved_until}))
        conn.commit()

        logging.info(cursor.query)
        logging.info(cursor.statusmessage)

        if cursor.statusmessage != "UPDATE 0":
            return 'Reservation done'

        else:
            return "Couldn't reserve item"
    else:
        return ''


def reserve_room(room: Room, user: User, time: Time):
    cursor, conn = test_con_db.set_cursor()
    if cursor:
        room.rid = UUID_adapter(room.rid) 

        cursor.execute("""
                            UPDATE rooms
                            SET reserved_by = %(reserved_by)s, reserved_from = %(datefrom)s, reserved_until = %(dateuntil)s
                            WHERE name = %(name)s AND id = %(rid)s AND reserved_by IS NULL AND reserved_from IS NULL AND reserved_until IS NULL; """,
                            ({'name': room.name, 'rid': room.rid, 'reserved_by': user.name + ':' + str(user.uid), 
                                'datefrom': time.reserved_from, 'dateuntil': time.reserved_until}))
        conn.commit()

        logging.info(cursor.query)
        logging.info(cursor.statusmessage)

        if cursor.statusmessage != "UPDATE 0":
            return 'Reservation done'

        else:
            return "Couldn't reserve item"
    else:
        return ''

def unreserve_item(item: Item, user: User):
    cursor, conn = test_con_db.set_cursor()     
    if cursor:
        item.iid = UUID_adapter(item.iid)
              

        cursor.execute("""
                            UPDATE items
                            SET reserved_by = NULL, reserved_from = NULL, reserved_until = NULL
                            WHERE name = %(name)s AND id = %(iid)s AND reserved_by = %(reserved_by)s AND reserved_from IS NOT NULL AND reserved_until IS NOT NULL; """,
                            ({'name': item.name, 'iid': item.iid, 'reserved_by': user.name + ':' + str(user.uid)}))
        conn.commit()

        logging.info(cursor.query)
        logging.info(cursor.statusmessage)

        if cursor.statusmessage != "UPDATE 0":
            return 'Reservation done'

        else:
            return "Couldn't reserve item"
    else:
        return ''


def unreserve_room(room: Room, user: User):
    cursor, conn = test_con_db.set_cursor()
    if cursor:
        room.rid = UUID_adapter(room.rid) 

        cursor.execute("""
                            UPDATE rooms
                            SET reserved_by = NULL, reserved_from = NULL, reserved_until = NULL
                            WHERE name = %(name)s AND id = %(iid)s AND reserved_by = %(reserved_by)s AND reserved_from IS NOT NULL AND reserved_until IS NOT NULL; """,
                            ({'name': room.name, 'rid': room.rid, 'reserved_by': user.name + ':' + str(user.uid)}))

        conn.commit()

        logging.info(cursor.query)
        logging.info(cursor.statusmessage)

        if cursor.statusmessage != "UPDATE 0":
            return 'Reservation done'

        else:
            return "Couldn't reserve item"
    else:
        return ''