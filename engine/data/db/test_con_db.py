import psycopg2, time, configparser, os, logging

dir_path = os.path.dirname(os.path.realpath(__file__))
config = configparser.ConfigParser()

config.read(dir_path + '/../conf/config.ini')   # or use env variables for config

rootLogger = logging.getLogger()

def start():

    connection = False

    try:
        connection = psycopg2.connect(user = config['postgresqlDB']['user'],            
                                    password = config['postgresqlDB']['password'],
                                    host = config['postgresqlDB']['host'],
                                    port = config['postgresqlDB']['port'],
                                    database = config['postgresqlDB']['database'])

        cursor = connection.cursor()
        # Print PostgreSQL Connection properties
        rootLogger.info(connection.get_dsn_parameters())

        # Print PostgreSQL version
        cursor.execute("SELECT version();")
        record = cursor.fetchone()

        rootLogger.info("You are connected to - " + str(record))


    except (Exception, psycopg2.Error) as error :
        rootLogger.info("Error while connecting to PostgreSQL" + str(error))

    return connection


def set_cursor():
    conn = start()
    if conn:
        rootLogger.info('Cursor set successfully')
        cursor = conn.cursor()
        return cursor, conn
    else:
        rootLogger.error('Cannot set cursor')
        return None