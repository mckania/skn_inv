from fastapi import FastAPI
from pydantic import BaseModel, ValidationError, root_validator
import datetime, uuid

""" classes for request's body """

class Item(BaseModel):
    name: str
    iid: uuid.UUID

class Room(BaseModel):
    name: str
    rid: uuid.UUID

class User(BaseModel):
    name: str
    uid: uuid.UUID

class Response(BaseModel):
    information: str

class Time(BaseModel):
    reserved_from: str
    reserved_until: str

    @root_validator
    def convert_to_datetime(cls, values):
        rf, ru = values.get('reserved_from'), values.get('reserved_until')
        if rf is not None and ru is not None:
            try:
                rf = datetime.datetime.strptime(rf, '%Y-%m-%d %H:%M:%S')
                ru = datetime.datetime.strptime(ru, '%Y-%m-%d %H:%M:%S')
                if rf < ru:
                    return values
                else:
                    raise(ValueError("wrong dates"))
            except ValueError as e:
                print(e)

def uuid_to_str(uuid: uuid.UUID):
    return str(uuid)