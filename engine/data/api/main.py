from fastapi import FastAPI, status
from db import get_from_db, post_to_db
from db.request_items import *
import logging, sys

"""All endpoints of API"""

"""Creates logger for all filles"""

logFormatter = logging.Formatter("%(asctime)s [%(threadName)-12.12s] [%(levelname)-5.5s]  %(message)s")
rootLogger = logging.getLogger()

rootLogger.setLevel(level=logging.INFO)

consoleHandler = logging.StreamHandler(sys.stdout)
consoleHandler.setFormatter(logFormatter)
fileHandler = logging.FileHandler('logs_detailed.log')
fileHandler.setFormatter(logFormatter)

rootLogger.addHandler(consoleHandler)
rootLogger.addHandler(fileHandler)

app = FastAPI(docs_url="/")

@app.get('/tables')
async def tables():
    return get_from_db.get_tables()

@app.get('/items')
async def items():
    return get_from_db.get_items()

@app.get('/rooms')
async def rooms():
    return get_from_db.get_rooms()

@app.get('/item/{id}')
async def item_by_id(id: str):
    return get_from_db.get_item_by_id(id)

@app.get('/room/{id}')
async def room_by_id(id: str):
    return get_from_db.get_room_by_id(id)

@app.get('/reserved')
async def get_reserved():
    return get_from_db.get_reserved()

@app.get('/unreserved')
async def get_unreserved():
    return get_from_db.get_unreserved()

@app.put('/reserve_item')
async def reserve_item(item: Item, user: User, time: Time):
    return post_to_db.reserve_item(item, user, time)

@app.put('/reserve_room')
async def reserve_room(room: Room, user: User, time: Time):
    return post_to_db.reserve_room(room, user, time)

@app.put('/unreserve_item')
async def unreserve_item(item: Item, user: User):
    return post_to_db.unreserve_ioom(item, user)

@app.put('/unreserve_room')
async def unreserve_room(room: Room, user: User):
    return post_to_db.unreserve_room(room, user)


