# API engine for items reservation at the SKN Telephoners site

## Overview

### This python application uses FastAPI framework for exposing API endpoints that can be used by any server for items and rooms reservation. This engine is connecting to the PostgreSQL database where it proceed with reservation on the persistent storage.


## How to use
### ``` docker-compose up ``` 
or 
### ``` docker-compose build && docker-compose start```

## API docs 
### ```http://localhost:8080/``` 
### ```http://localhost:8080/redoc```

## Structure
### ```db/``` contains psql Docker image with sample table for testing
### ```engine/``` contains Docker image for our engine with conections details to DB located in ```engine/data/conf/config.ini```

![architecture](api.png)
