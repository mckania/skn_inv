--
-- PostgreSQL database dump
--

-- Dumped from database version 12.2 (Debian 12.2-2.pgdg100+1)
-- Dumped by pg_dump version 12.2 (Debian 12.2-2.pgdg100+1)

SET statement_timeout = 0;
SET lock_timeout = 0;
SET idle_in_transaction_session_timeout = 0;
SET client_encoding = 'UTF8';
SET standard_conforming_strings = on;
SELECT pg_catalog.set_config('search_path', '', false);
SET check_function_bodies = false;
SET xmloption = content;
SET client_min_messages = warning;
SET row_security = off;

--
-- Name: uuid-ossp; Type: EXTENSION; Schema: -; Owner: -
--

CREATE EXTENSION IF NOT EXISTS "uuid-ossp" WITH SCHEMA public;


--
-- Name: EXTENSION "uuid-ossp"; Type: COMMENT; Schema: -; Owner:
--

COMMENT ON EXTENSION "uuid-ossp" IS 'generate universally unique identifiers (UUIDs)';


SET default_tablespace = '';

SET default_table_access_method = heap;

--
-- Name: auth_user; Type: TABLE; Schema: public; Owner: skn_inv
--

CREATE TABLE public.auth_user
(
    uid  integer NOT NULL,
    name character varying(255)
);


ALTER TABLE public.auth_user
    OWNER TO skn_inv;

--
-- Name: reservables; Type: TABLE; Schema: public; Owner: skn_inv
--

CREATE TABLE public.reservables
(
    name    character varying(255),
    rid     uuid    DEFAULT public.uuid_generate_v4() NOT NULL,
    is_room boolean DEFAULT false
);


ALTER TABLE public.reservables
    OWNER TO skn_inv;

--
-- Name: reservations; Type: TABLE; Schema: public; Owner: skn_inv
--

CREATE TABLE public.reservations
(
    id        uuid DEFAULT public.uuid_generate_v4() NOT NULL,
    rid       uuid,
    uid       integer,
    time_from timestamp with time zone               NOT NULL,
    time_to   timestamp with time zone               NOT NULL
);


ALTER TABLE public.reservations
    OWNER TO skn_inv;

--
-- Data for Name: auth_user; Type: TABLE DATA; Schema: public; Owner: skn_inv
--

COPY public.auth_user (uid, name) FROM stdin;
1	test_user1
2	test_user2
\.


--
-- Data for Name: reservables; Type: TABLE DATA; Schema: public; Owner: skn_inv
--

COPY public.reservables (name, rid, is_room) FROM stdin;
test_item1	0eb4ffc8-e31a-497b-b66e-a99bc1769713	f
test_item2	fee7818c-1b1e-4de1-956c-70c8b6d7a4fd	f
test_room1	cd782694-6f58-474c-87a5-236d4996879e	t
test_room2	7076803b-349b-4b5c-bc7c-6939899cf4bf	t
\.


--
-- Data for Name: reservations; Type: TABLE DATA; Schema: public; Owner: skn_inv
--

COPY public.reservations (id, rid, uid, time_from, time_to) FROM stdin;
cd08b677-15af-42e5-b1d4-fb609f82c423	0eb4ffc8-e31a-497b-b66e-a99bc1769713	1	2020-06-13 10:00:00+00	2020-06-20 09:30:40+00
\.


--
-- Name: auth_user auth_user_pkey; Type: CONSTRAINT; Schema: public; Owner: skn_inv
--

ALTER TABLE ONLY public.auth_user
    ADD CONSTRAINT auth_user_pkey PRIMARY KEY (uid);


--
-- Name: reservables reservables_pkey; Type: CONSTRAINT; Schema: public; Owner: skn_inv
--

ALTER TABLE ONLY public.reservables
    ADD CONSTRAINT reservables_pkey PRIMARY KEY (rid);


--
-- Name: reservations reservations_pkey; Type: CONSTRAINT; Schema: public; Owner: skn_inv
--

ALTER TABLE ONLY public.reservations
    ADD CONSTRAINT reservations_pkey PRIMARY KEY (id);


--
-- Name: reservations reservations_rid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: skn_inv
--

ALTER TABLE ONLY public.reservations
    ADD CONSTRAINT reservations_rid_fkey FOREIGN KEY (rid) REFERENCES public.reservables (rid);


--
-- Name: reservations reservations_uid_fkey; Type: FK CONSTRAINT; Schema: public; Owner: skn_inv
--

ALTER TABLE ONLY public.reservations
    ADD CONSTRAINT reservations_uid_fkey FOREIGN KEY (uid) REFERENCES public.auth_user (uid);


--
-- PostgreSQL database dump complete
--
